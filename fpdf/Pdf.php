<?php
class PDF extends FPDF {

    public function HeaderPdf($image, $code) {
        // Image
        $this->Image('public/images/'.$image ,30,32,150);
        // Police Arial gras 15
        $this->SetFont('Arial','B',15);
        // Décalage à droite
        $this->Cell(80);
        // Code
        $this->Cell(30,115,$code,'C');
        // Saut de ligne
        $this->Cell(30,115,$code,'C');
        // Saut de ligne
        $this->Ln(20);
    }
}
