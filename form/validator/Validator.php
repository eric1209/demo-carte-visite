<?php

class Validator {

public static function checkField($field) {
    if (empty($field)) {
        throw new InvalidFieldException("Invalid argument field");
    }
    return true;
}

public static function checkEmail($email) {
    if ((!filter_var($email, FILTER_VALIDATE_EMAIL)) && (!empty($email))) {
        throw new InvalidEmailException("Invalid argument email");
    }
    return true;
}
public function checkCard($cardSelected) {

if ($cardSelected === "blue") {
        $image = "carte-blue.png";
    }
    if ($cardSelected === "green") {
        $image = "carte-green.png";
    }
    if ($cardSelected === "orange") {
        $image = "carte-orange.png";
    }
    if ($cardSelected === "pink") {
        $image = "carte-pink.png";
    }
    if (!isset($image)) {
        throw new InvalidCardException("Invalid argument card");
    }
    return $image;
}

public function skip_accents( $str, $charset='utf-8' ) {

    $str = htmlentities( $str, ENT_NOQUOTES, $charset );

    $str = preg_replace( '#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str );
    $str = preg_replace( '#&([A-za-z]{2})(?:lig);#', '\1', $str );
    $str = preg_replace( '#&[^;]+;#', '', $str );

    return $str;

}

}
