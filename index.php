<?php
require('controller/frontend.php');

try {
    if (isset($_GET['message'])) {
        goResult();
    } else if (isset($_GET['download'])){
        downloadPdf();
    } else {
        home();
    }
} catch (Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
    require('view/frontend/errorView.php');
}
