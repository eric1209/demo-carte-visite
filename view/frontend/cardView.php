<?php $title = "carte de visite"; ?>

<?php ob_start(); ?>

<div class="container mb-4">
    <h1 class="mt-4 mb-5">Création d'une carte de visite</h1>
    <p class="mb-5">Ceci est une démonstration, remplissez le formulaire afin de créer une carte de visite !</p>
    <p class="center"><a href="https://gitlab.com/eric1209/demo-carte-visite/tree/master/" target="_blank"><img class="gitab-img" src="public/images/gitlab.png" alt="lien gitlab"></a></p>
    <p class="center">cliquez sur l'image pour voir le repository</p>
    <form  action="index.php" method="post">
        <div class="form-group">
            <?php if (isset($invalidField)) { echo "<p class=\"error\">" . $invalidField . "</p>"; } ?>
            <?php if (isset($invalidEmail)) { echo "<p class=\"error\">" . $invalidEmail . "</p>"; } ?>
            <label for="lastName">Votre nom : </label>
            <input type="text" name="lastName" class="form-control" placeholder="lastName" <?php if(isset($lastName)) {echo "value=".$lastName;} ?>>
        </div>

        <label for="firstName">Votre prénom : </label>
        <input type="text" name="firstName" class="form-control" placeholder="firstName" <?php if(isset($firstName)) {echo "value=".$firstName;} ?>>

        <label for="email">Votre adresse mail : </label>
        <input type="text" name="email" class="form-control" placeholder="email" <?php if(isset($email)) {echo "value=".$email;} ?>>

        <span class="card">Choisissez votre modèle</span>

        <?php if (isset($invalidCard)) { echo "<p class=\"error\">" . $invalidCard . "</p>"; } ?>
        <div class="col-img">
                <img <?php if(isset($blue)) { echo "src=".$blue;} else {echo "src=\"public/images/web/carte-blue-web.jpg\"";}?> alt="demo" id="img-blue" class="img-card">
                <input class="form-control" type="radio" name="card" value="blue" id="blue" <?php if (isset($cardSelected) && $cardSelected === "blue") {echo "checked";} ?>>
        </div>
        <div class="col-img">
                <img <?php if(isset($green)) { echo "src=".$green;} else {echo "src=\"public/images/web/carte-green-web.jpg\"";}?> alt="demo" id="img-green" class="img-card">
                <input class="form-control" type="radio" name="card" value="green" id="green" <?php if (isset($cardSelected) && $cardSelected === "green") {echo "checked";} ?>>
        </div>
        <div class="col-img">
                <img <?php if(isset($orange)) { echo "src=".$orange;} else {echo "src=\"public/images/web/carte-orange-web.jpg\"";}?> alt="demo" id="img-orange" class="img-card">
                <input class="form-control" type="radio" name="card" value="orange" id="orange" <?php if (isset($cardSelected) && $cardSelected === "orange") {echo "checked";} ?>>
        </div>
        <div class="col-img">
                <img <?php if(isset($pink)) { echo "src=".$pink;} else {echo "src=\"public/images/web/carte-pink-web.jpg\"";}?> alt="demo" id="img-pink" class="img-card">
                <input class="form-control" type="radio" name="card" value="pink" id="pink" <?php if (isset($cardSelected) && $cardSelected === "pink") {echo "checked";} ?>>
        </div>

        <div class="form-group">
            <p>  <input type="submit" name="create" class="btn btn-primary" value="Valider"></p>
        </div>
    </form>
</div>


<?php $content = ob_get_clean();?>
<?php require("view/frontend/template.php"); ?>

<script>
$( "#blue" ).on( "click", function() {
    if($( "#blue" ).html( $( "input:checked" ).val() + " is checked!" )) {
        $('#img-blue').attr('src','public/images/web/carte-blue-web-checked.jpg');

        $('#img-green').attr('src','public/images/web/carte-green-web-trans.png');
        $('#img-orange').attr('src','public/images/web/carte-orange-web-trans.png');
        $('#img-pink').attr('src','public/images/web/carte-pink-web-trans.png');
    }
});
$( "#green" ).on( "click", function() {
    if($( "#green" ).html( $( "input:checked" ).val() + " is checked!" )) {
        $('#img-green').attr('src','public/images/web/carte-green-web-checked.jpg');

        $('#img-blue').attr('src','public/images/web/carte-blue-web-trans.png');
        $('#img-orange').attr('src','public/images/web/carte-orange-web-trans.png');
        $('#img-pink').attr('src','public/images/web/carte-pink-web-trans.png');
    }
});
$( "#orange" ).on( "click", function() {
    if($( "#orange" ).html( $( "input:checked" ).val() + " is checked!" )) {
        $('#img-orange').attr('src','public/images/web/carte-orange-web-checked.jpg');

        $('#img-blue').attr('src','public/images/web/carte-blue-web-trans.png');
        $('#img-green').attr('src','public/images/web/carte-green-web-trans.png');
        $('#img-pink').attr('src','public/images/web/carte-pink-web-trans.png');
    }
});
$( "#pink" ).on( "click", function() {
    if($( "#pink" ).html( $( "input:checked" ).val() + " is checked!" )) {
        $('#img-pink').attr('src','public/images/web/carte-pink-web-checked.jpg');

        $('#img-blue').attr('src','public/images/web/carte-blue-web-trans.png');
        $('#img-green').attr('src','public/images/web/carte-green-web-trans.png');
        $('#img-orange').attr('src','public/images/web/carte-orange-web-trans.png');
    }
});
</script>
