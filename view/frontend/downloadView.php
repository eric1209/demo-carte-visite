<?php $title = "télécharger"; ?>

<?php ob_start(); ?>

<div class="container mh">

    <h1 class="mb-5 mt-5">Votre carte de visite</h1>

    <?php if (!isset($validated)): ?>
        <p class="center mb-5">Lien indisponible, retournez à l'accueil pour recréer une carte !</p>
        <p class="center"><a title="demo carte de visite" href="index.php" class="btn btn-secondary">Retour à la démo</a></p>
    <?php endif; ?>

    <?php if (isset($validated)): ?>
        <p class="center mb-5">Vous pouvez télécharger votre carte de visite !</p>
        <p class="center"><a id="tel" class="btn btn-primary mb-5" href="pdf/<?php echo "carte-visite-" . $date . "-" . $tokenOk . ".pdf"; ?>" download>télécharger</a></p>
        <p class="center"><a title="demo carte de visite" href="index.php" class="btn btn-secondary">Retour à la démo</a></p>
    <?php endif; ?>
</div>
<?php $content = ob_get_clean();?>
<?php require("view/frontend/template.php"); ?>
