<?php $title = "carte envoyé"; ?>

<?php ob_start(); ?>

<div class="container mh">

    <h1 class="mb-5 mt-5">Carte de visite créée</h1>

    <p class="center mb-5">Pour télécharger votre carte, consulter votre voite email. Si vous n'avez rien reçu, veullez consulter votre courrier indésirable ou spam</p>

    <p class="center"><a title="demo carte de visite" href="index.php" class="btn btn-secondary">Retour à la démo</a></p>
</div>
<?php $content = ob_get_clean();?>
<?php require("view/frontend/template.php"); ?>
