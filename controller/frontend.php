<?php

require('fpdf/fpdf.php');
require('form/validator/Validator.php');
require('form/exception/InvalidFieldException.php');
require('form/exception/InvalidEmailException.php');
require('form/exception/InvalidCardException.php');
require('email/Email.php');

function home() {
    if (isset($_POST['create'])) {

        try {

            $lastName = htmlspecialchars(strip_tags($_POST['lastName']));
            $firstName = htmlspecialchars(strip_tags(trim($_POST['firstName'])));
            $email = htmlspecialchars(strip_tags(trim($_POST['email'])));

            if (isset($_POST['card'])) {
                $cardSelected = htmlspecialchars(strip_tags(trim($_POST['card'])));
            if ($_POST['card'] === "blue") {
                $blue = "public/images/web/carte-blue-web-checked.jpg";
            }
            if ($_POST['card'] === "orange") {
                $orange = "public/images/web/carte-orange-web-checked.jpg";
            }
            if ($_POST['card'] === "pink") {
                $pink = "public/images/web/carte-pink-web-checked.jpg";
            }
            if ($_POST['card'] === "green") {
                $green = "public/images/web/carte-green-web-checked.jpg";
            }

            } else {
                $cardSelected = 0;
            }

            Validator::checkField($lastName);
            Validator::checkField($firstName);
            Validator::checkField($email);
            Validator::checkEmail($email);

            $validor = New Validator;
            $image = $validor->checkCard($cardSelected);
            $firstName = $validor->skip_accents($firstName, 'utf-8');
            $lastName = $validor->skip_accents($lastName, 'utf-8');

            $token = uniqid("demo", true);

            // suppression des fichiers dont la date ne correspond pas a now()
            $date = date("d-m-y");
            $files = glob('pdf/*.{pdf}', GLOB_BRACE);

            foreach ($files as $key => $file) {
                $createdAt = substr($file, -40, 8);
                if ($createdAt != $date) {
                    unlink($file);
                }
            }

            $pdf = new FPDF();
            $pdf->AliasNbPages();
            $pdf->AddPage();
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(60);
            $pdf->Image('public/images/' . $image,65,52,80);
            $pdf->Cell(100,172,$firstName . " " . $lastName,'C');
            $pdf->Ln();
            $pdf->Cell(68);
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(100,-235, "Fantastique !",'C');
            $pdf->Ln();
            $pdf->Cell(40);
            $pdf->SetFont('Arial','',24);
            $pdf->SetTextColor(120,125,133);
            $pdf->Cell(180,136, "Votre nouvelle carte de visite",'C');
            $pdf->Ln();
            $pdf->Cell(67);
            $pdf->SetFont('Arial','',9);
            $pdf->SetTextColor(120,125,133);
            $pdf->Cell(160,-15, "Voici votre carte ;-)",'C');
            $pdf->Ln();
            $pdf->Cell(75);
            $pdf->SetFont('Arial','',12);
            $pdf->SetTextColor(120,125,133);
            $pdf->Cell(160,-85, "www.eric-tasca.fr",'C');

            $pdf->Output('F','pdf/carte-visite-' . $date . '-' . $token . '.pdf');

            $emailToUser = New Email;
            $emailToUser->sendEmail($email, $firstName, $lastName);
            $emailToUser->sendEmailToUser($email, $firstName, $lastName, $token);

            header("Location: index.php?message&f=" . $firstName . "&l=" . $lastName . "&token=" . $token );

        } catch (InvalidFieldException $e) {
        $invalidField = "Vous devez remplir tous les champs !";
        } catch (InvalidEmailException $e) {
        $invalidEmail = "Vous devez entrer une adresse email valide !";
        }  catch (InvalidCardException $e) {
        $invalidCard = "Vous devez choisir un modèle de carte !";
        } catch (\Exception $e) {
        echo "Quelque chose d'imprévue c'est passé ";
        //echo $e->getMessage();
        exit();
        }

    }
    require('view/frontend/cardView.php');
}

function goResult()
{
    if (!isset($_GET['l']) || empty($_GET['l'])) {
      header("Location: index.php" );
    } else {
        $lastName = $_GET['l'];
    }
    if (!isset($_GET['f']) || empty($_GET['f'])) {
      header("Location: index.php" );
    } else {
        $firstName = $_GET['f'];
    }
    if (!isset($_GET['token']) || empty($_GET['token'])) {
      header("Location: index.php" );
    } else {
        $token = $_GET['token'];
    }
    $date = date("d-m-y");

    $files = glob('pdf/*.{pdf}', GLOB_BRACE);
    foreach ($files as $key => $file) {
        if ($file === "pdf/carte-visite-" . $date . "-" . $token . ".pdf") {
            $isExist = 1;
        }
    }

    if (!isset($isExist)) {
        header('Location: index.php');
    }

    require('view/frontend/resultView.php');
}


function downloadPdf() {
    if (!isset($_GET['token'])) {
        header('Location: index.php');
    }  else {
        $token = $_GET['token'];
    }
    $date = date("d-m-y");
    $files = glob('pdf/*.{pdf}', GLOB_BRACE);

    foreach ($files as $key => $file) {
        $tokenOk = substr($file,-31, 27);
        $createdAt = substr($file,-40, 8);
        if ($date != $createdAt) {
            unlink("pdf/carte-visite-" . $createdAt ."-" . $tokenOk . ".pdf");
        }
        if ($token === $tokenOk) {
            $validated = 1;
        }
    }
    require('view/frontend/downloadView.php');
}
