<?php
class Email {
  public function sendEmail($email, $firstName, $lastName) {

    $to = "eric.tasca@hotmail.fr";
    $subject = "demo carte de visite";
    $from = "erictasca.contact@gmail.com";
    $headers = 'From: erictasca.contact@gmail.com' . "\r\n" .
     'Reply-To: erictasca.contact@gmail.com' . "\r\n" .
     'X-Mailer: PHP/' . phpversion();

    $message ="Bonjour, " .
        $firstName . " " . $lastName .
        " a fait votre demo carte de visite.
        contact : ". $email
    ;

    mail($to, $subject, $message, $headers);
  }

  public function sendEmailToUser($email, $firstName, $lastName, $token) {

    $to = $email;
    $subject = "demo carte de visite";
    $from = "erictasca.contact@gmail.com";
    $headers = 'From: erictasca.contact@gmail.com' . "\r\n" .
     'Reply-To: erictasca.contact@gmail.com' . "\r\n" .
     'X-Mailer: PHP/' . phpversion();

    $message ="Bonjour " .
        $firstName . " " . $lastName .
        ", cliquez sur le lien ci dessous pour obtenir votre carte :
        http://www.eric-tasca.fr/demo-carte-visite/index.php?download&token=" . $token . "
    ";

    mail($to, $subject, $message, $headers);
  }

}
